import React, {Component,component} from 'React';
import App from './App';

class  DisplayInformation extends component{
    render(){
        return(
            <div>
                <h1>Information</h1>
                <h2>My name is {this.props.fullname}</h2>
                <h2>{this.props.address}</h2>
                <h2>{this.props.phone}</h2>
            </div>
        )
    }
}
export default App;